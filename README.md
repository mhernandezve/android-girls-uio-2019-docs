# ANDROID GIRLS 2019, QUITO, ECUADOR

# Que es Android?

Es un sistema operativo basado en linux, el cual es usado para dispositivos móviles. Actualmente es el sistema operativo más utilizado en el mundo. No vamos a ahondar en su estructura, vamos a entrar directo al desarrollo de aplicaciones, que es por lo que estamos aquí.

Como primer punto, vamos a ver cómo podemos organizar nuestro código, existen varias maneras de hacerlo, ya que Android no provee una arquitectura mandatoria, así que les nombraremos algunas para que puedan revisarlas si algún momento quieren utilizar una diferente.

Tenemos:

* **MVVM**: Model View View Model (Recomendada por Android)
* **VIPER**: View Interactor Presenter Entity Routing (Muy útil para proyectos que son medianos y grandes).
* **MVC**: Model View Controller
* **MVP**: Model View Presenter (Útil para aplicaciones pequeñas y medianas).

Este es un buen artículo que puedes leer si quieres entender todas: https://proandroiddev.com/mvc-mvp-mvvm-clean-viper-redux-mvi-prnsaaspfruicc-building-abstractions-for-the-sake-of-building-18459ab89386

Las arquitecturas facilitan la organización de código, el crecimiento de las aplicaciones y la facilidad de probar nuestras aplicaciones.

Hoy vamos a usar MVP. La elegimos esta vez, porque es una arquitectura fácil de entender y permite mostrar fácilmente todas las capas de una aplicación, además que estamos muy seguros que después de esta arquitectura, la aplicación de cualquier otra va a ser mucho más sencilla.

---

### MVP: Una breve explicación

La capa **Model** es la encargada de manejar nuestras entidades y las acciones que con ellas se realizan. Guardarlas, recuperarlas, actualizarlas. En esta capa tendremos nuestros modelos(entidades), repositorios, servicios.

La capa **Presenter** podría decirse que es la capa más crucial, ya que esta capa es la encargada de manejar la lógica de negocio de nuestra aplicación. Básicamente recibe datos y acciones de la capa view, realiza las acciones necesarias con estos datos y los manda a la capa model para que maneje estos datos, una vez que tiene una respuesta, maneja los datos nuevamente y los pasa a los views. Entonces podemos ver el porqué esta arquitectura no es usada en aplicaciones grandes, el presenter se vuelve muy grande y difícil de manejar y separar.

La capa **View**, es la capa que muestra información al usuario, maneja las interfaces, aquí es donde están nuestros activities, fragments, adapters, view holders, entre otras. Cabe recalcar que esta capa no debe tener lógica, solo debe manejar las interfaces, esto hará posible que todo esté correctamente probado en nuestra aplicación, ya que esta capa es muy difícil de testear de manera unitaria.La ecomendación sería que la capa de presentación sea testeada solo de manera funcional.

---

Una de las cosas esenciales que hay que entender en Android, es el ciclo de vida. En esta ocasión solo vamos a revisar el ciclo de vida de los activities, ya que esto es lo que más van a utilizar. El siguiente gráfico ilustra cómo está compuesto el ciclo de vida:

## Ciclo de vida de Android

<div align="center"><img src="./images/activity_lifecycle.png"/></div>

El primer método, y el cual obligatoriamente vas a tener que sobreescribir, es el `OnCreate`, el cual se ejecuta al empezar un activity. Como te podrás imaginar, no es adecuado poner cosas que bloqueen el hilo principal, ya que vas a tener una pantalla en blanco por un periodo de tiempo (malo, muy malo).

El método `onStart` se ejecuta seguido del `onCreate`, en este método se ubican las cosas que quieres que se realicen en dependencia de lo que se hace en el onCreate. Se ejecuta en la inicialización  del activity.

El método `onResume` se ejecuta seguido del `onStart` en el inicio de la actividad y también se ejecuta después del `onPause` (se ejecuta cuando la aplicación entra en segundo plano) o cuando el activity está abierto esperando la respuesta de otro activity. Este método normalmente contiene código que debería ejecutarse sólo cuando el activity regresa del background. Hay que tener cuidado con lo que se pone aquí, si no se tiene cuidado lleva a comportamientos inesperados.

El método `onPause`, se ejecuta cuando el va a  regresar del  segundo plano. Este método es adecuado para persistir el estado de la aplicación.
 
El método `onStop` se ejecuta cuando una aplicación es finalizada. Se puede realizar acciones para guardar datos del usuario, cerrar sesiones o acciones parecidas.

El método `onDestroy` es un método que se ejecuta cuando la actividad es cerrada. Luego de que se llama al método finish(sirve para sacar cerrar definitivamente un activity).

El método `onRestart` se ejecuta justo antes de que una aplicación vuelva al primer plano, justo antes de que vuelva a iniciarse.

Toda la documentación la pueden ver aquí: https://developer.android.com/guide/components/activities?hl=es-419

Una vez hecho esto, vamos a empezar con nuestra aplicación!! Yay!

# Aplicación

Esta aplicación es sobre un catálogo de películas, la vamos a ir haciendo de manera incremental, para poder visualizar cómo se manejan las cosas en una aplicación Android.

Nuestro primer paso será crear la aplicación desde 0, para lo cual vamos a abrir nuestro IDE, llamado Android Studio.

Damos click en Start a new Android Studio Project

<div align="center"><img src="./images/welcome-to-android-studio.png"/></div>

En esta pantalla nos da la opción de crear un template para la aplicación. Seleccionaremos la opción **Empty Activity**.

<div align="center"><img src="./images/choose-your-project.png"/></div>

En esta pantalla vamos a definir el nombre (sugerido MyMovies), paquete (sugerido com.android.girls) y ubicación de nuestra aplicación. Asimismo elegimos Kotlin como lenguaje de desarrollo y la versión mínima de Android, que en este caso lo haremos desde el **API 21**. Esto con el motivo de poder hacer uso de las librerías más actuales.

<div align="center"><img src="./images/configure-your-project.png"/></div>

Si hacen click en el hiperlink Help me choose, se muestra un cuadro en el que pueden ver el porcentaje de cobertura de dispositivos que tendrá su aplicación. Esto es importante definirlo al inicio del desarrollo, ya que después los problemas de compatibilidad podrían tomar mucho más tiempo de desarrollo.

<div align="center"><img src="./images/api-version-distribution.png"/></div>

Finalmente damos clic en Finish para crear nuestro esqueleto base para empezar a desarrollar.

<div align="center"><img src="./images/project-structure-android-manifest.png"/></div>

La primera carpeta `manifest` contiene el archivo `AndroidManifest.xml`. Este archivo es donde se deben registrar todas las pantallas que van a estar visibles en la aplicación  (se podría usar como un feature toggle), además es el lugar donde se agregan otras configuraciones como permisos, nombre de la aplicación.  Es nuestro archivo de configuración principal.

<div align="center"><img src="./images/project-structure-java.png"/></div>

La primera carpeta dentro de `java`, que tiene el nombre del paquete que definimos contiene el código fuente (en este caso Kotlin), que vamos a usar para desarrollar las funcionalidades de nuestra aplicación. Activities, servicios, modelos, presenters, etc., van dentro de esta carpeta.

La segunda carpeta, que tiene el nombre `androidTest` entre paréntesis, es donde vamos a poner nuestras pruebas de instrumentación (End to End), son pruebas que necesitan la aplicación corriendo. Por defecto están configuradas con Espresso (esperamos hacer un taller de esto más adelante).

La tercera carpeta, que tiene el nombre test entre paréntesis, es el lugar donde se van a poner nuestras pruebas unitarias. Por defecto funcionan con Junit. 

Pero porqué están divididas en 3 carpetas? Vamos a ver nuestro archivo de dependencias `build.gradle`.

<div align="center"><img src="./images/project-structure-build-gradle.png"/></div>

Como se puede apreciar en la imagen, ahí es donde se ubica este archivo. Contiene las dependencias y tareas que queremos configurar para nuestro proyecto. Aquí hay varias cosas interesantes:

* `CompileSdkVersion`: Es la versión del SDK de Android con el que nuestra aplicación va a ser desarrollada y construida.
* `applicationId`: Este Id es el que diferencia las aplicaciones en los dispositivos. Es decir, en un mismo dispositivo puedes tener n aplicaciones con el mismo nombre, pero con diferentes Ids. Este es único para cada aplicación. Si es el mismo, buscará reemplazar la aplicación existente al momento de la instalación.
* `minSdkVersion`: La versión mínima de Android en la que nuestra aplicación va a funcionar. Si el dispositivo tiene una versión menor, la aplicación no va a aparecer en Google Play.
* `targetSdkVersion`: Esta variable define la versión para cual tu aplicación tiene funcionalidad 100% comprobada. Es decir, que si aquí ponemos la versión 27, funcionará en la 28 pero no todo está 100% probado y compatible.
* `versionCode`: Este es el código de la versión de la aplicación. Necesario para la tienda de Android. Debe ser un número entero que va a ser superior para cada release. No es visible a los usuarios finales.
* `versionName`: Es el número de la versión que va a ser mostrado a los usuarios en la tienda.
* `testInstrumentationRunner`: Especifica con qué Framework van a funcionar las pruebas de instrumentación.
* `buildTypes`: Especifica que builds va a tener nuestra aplicación. Por defecto tiene solo release, pero podemos poner otros tipos de build que por ejemplo tengan la posibilidad de ser debuggeados. No se debe confundir con flavors, cada flavor de hecho va a tener los build types definidos en este file.
* `dependencies`: Esta es una de las más (sino la más relevante), aquí se ponen las dependencias. Las que van bajo la palabra implementation son las dependencias que van a ser incluidas en el APK (Android Application Package), que es el instalador de una aplicación Android. Las dependencias precedidas por la palabra `testImplementation` son las cuales van a estar disponibles sólo en el contexto de pruebas unitarias. Y las dependencias que van acompañadas de la palabra `androidTestImplementation` son las que solo estarán disponibles en el contexto de pruebas de instrumentación. 

Y pues, estas definiciones ayudan a que la compilación sea más rápida para cada instancia.

Ahora vamos a ver la carpeta `res`.

<div align="center"><img src="./images/project-structure-resources.png"/></div>

1. La carpeta `drawable` es donde se guardan los recursos visuales para la aplicación, como las imágenes que van a estar en la aplicación. PNG's, SVG's, JPG's son algunos de los recursos válidos.
2. La carpeta `layout` es una carpeta en donde se ponen todos los xml's que representan las interfaces visuales de la aplicación. 
3. La carpeta `mipmap` contiene los íconos de la aplicación, los cuales se muestran una vez que la aplicación es instalada.
4. La carpeta `values` tiene 3 archivos relevantes:
	* `Strings`: En este archivo deben ponerse todos los recursos de texto que se van a mostrar en la aplicación. 
	* `Colors`: En este archivo van todos los colores que se quiera usar en la aplicación. Ayuda mucho a la organización y la no repetición de estos recursos.
	* `Styles`: En este archivo se definen estilos visuales para componentes. Es recomendable que lo que esté en estos archivos sea algo que se esté reutilizando. 

Vamos a dar un vistazo rápido a los dispositivos virtuales:

<div align="center"><img src="./images/avd-manager-icon.png"/></div>

Android Studio nos da la posibilidad de crear dispositivos virtuales (**A**ndroid **V**irtual **D**evice), los cuales nos permiten simular los sistemas operativos de Android en nuestra máquina. Son muy útiles para testear nuestras aplicaciones, pero hay que tener en cuenta que no se puede asegurar que el aspecto de nuestra aplicación en el AVD sea el mismo que en un dispositivo real.

Ahora vamos a ver cómo se crean los dispositivos a través del IDE.

<div align="center"><img src="./images/avd-manager-create-device.png"/></div>

Hacemos clic en **Create Virtual Device**. Una vez hecho esto, nos aparecerá lo siguiente:

<div align="center"><img src="./images/avd-manager-hardware.png"/></div>

En esta pantalla escogemos el dispositivo que queremos simular. Esto es para escoger dispositivos de diferentes tamaños y resoluciones de pantalla. Como pueden ver en el menú de la izquierda, se puede crear televisiones, relojes o tablets. Escojan el dispositivo que más les guste.

Una vez que damos clic en siguiente, podemos ver las versiones de sistema operativo. 

<div align="center"><img src="./images/avd-manager-system-image.png"/></div>

Vamos a descargar la versión más actual (si es que no la tienen ya). Una vez descargada damos clic en Next.

<div align="center"><img src="./images/avd-manager-sdk-quickfix.png"/></div>

La última pantalla permite dar un nombre a nuestro dispositivo y algunas configuraciones secundarias.

<div align="center"><img src="./images/avd-manager-verify-config.png"/></div>

Damos click en finish y el dispositivo quedará finalmente creado.

<div align="center"><img src="./images/avd-manager-your-virtual-devices.png"/></div>

Para iniciar el dispositivo damos click en el ícono indicado y listo. Demora un poco en iniciar la primera vez, pero después de eso funciona muy bien. Si quieren probar con sus dispositivos personales, también está muy bien.

<div align="center"><img src="./images/virtual-device-desktop.png"/></div>

Vamos a correr nuestra primera App.

<div align="center"><img src="./images/run-app.png"/></div>

Damos click en el ícono del play. Una vez hecho esto, seleccionamos el dispositivo en el que queremos que corra nuestra aplicación.

<div align="center"><img src="./images/select-deploy-target.png"/></div>

Damos click en Ok. Y veremos nuestra aplicación desplegada en el dispositivo.

<div align="center"><img src="./images/deployed-app.png"/></div>

Vamos a hacer un cambio rápido. Nos vamos a dirigir a la carpeta layouts dentro de res. Una vez ahí, abrimos el archivo activity_main.xml. Vamos a cambiar el texto que dice Hello World! Por nuestro nombre.

<div align="center"><img src="./images/changing-hello-world-text.png"/></div>

Una vez hecho esto, vamos a hacer clic en el ícono indicado en la imagen:

<div align="center"><img src="./images/hot-reload-icon.png"/></div>

Ese ícono nos permite desplegar cambios visuales (de UI, solo XML), sin necesidad de compilar nuestra aplicación nuevamente. Es muy útil al momento de diseñar las pantallas de nuestra aplicación. Probémoslo.

<div align="center"><img src="./images/updated-hello-world.png"/></div>

Ok. Creo que conocemos lo necesario para comenzar con nuestra aplicación Android. Ahora vamos a empezar con una aplicación que está en un repositorio Git que es sobre la cual vamos a trabajar.

(Coaches por favor hacer una breve introducción de Git si es que no lo conocieran las participantes.)

Vamos al terminal y copiamos lo siguiente:

``` bash
cd Documents
git clone https://github.com/JorgeRodriguez21/MyMovies.git
cd MyMovies
git fetch
git checkout 2019-version
git checkout 26270ab3c5e54af4604acaf607e666f3c8f6c68e
```

Una vez hecho esto, tendremos el repositorio con la estructura de carpetas necesaria para trabajar en una aplicación con la arquitectura MVP. Ahora, vamos a comenzar con el desarrollo de la aplicación.
Abre tu proyecto en Android Studio y vamos a programar!

La aplicación que vamos a realizar, es una aplicación sobre un catálogo de películas. Vamos a poder ver el listado de películas, ver el detalle de las mismas y quizá alguna acción más. 

Una vez abierto el proyecto, podrán ver la estructura de carpetas para un proyecto MVP (podría tener más).

<div align="center"><img src="./images/project-structure.png"/></div>

Vamos a empezar creando la clase Movie que es nuestro dominio principal:

En la clase movie que está dentro del paquete models, ponemos el siguiente bloque de código:

``` kotlin
package uio.androidbootcamp.moviesapp.model.models

import java.util.*

data class Movie (val id: Long = 0, val name: String = "", val posterPath: String = "", val overview: String = "", val releaseDate: Date = Date())
```

La clase Movie la trataremos como una data class, porque va a ser una clase sin lógica, es decir que va a ser puramente transaccional.

La aplicación la vamos a hacer incremental, es decir que primero vamos a hacer que la aplicación funcione hasta cierto punto aplicando unos conocimientos, y luego la vamos a completar con otras cosas que vamos a ir aprendiendo a lo largo de este taller.

## Hagamos un paréntesis para explicar algo bastante importante sobre Android:

Las aplicaciones móviles, son aplicaciones cuya principal característica es ser muy fluida. Para lograr esto, se necesita que el procesamiento sea rápido, que el usuario final no note que hay algo haciéndose tras la pantalla, es decir que cuando el usuario realice una acción, obtenga un feedback inmediato, no podemos permitir que una aplicación móvil se quede congelada, eso es un tema bastante importante. Es por esto, que en las aplicaciones móviles el trabajo casi siempre no es síncrono, es decir, no se ejecuta en el hilo principal, sino en un hilo paralelo y una vez que este trabajo esté terminado, la respuesta regresa al hilo principal, es procesada y mostrada al usuario. Esto se lo hace por medio de métodos llamados callbacks, cuya definición simple podría ser: métodos que se ejecutan cuando una acción específica termina su ejecución.

La arquitectura MVP, cuando se usa en Android, necesita de estos callbacks para poder funcionar correctamente de manera asíncrona. Todos estos callbacks estarán definidos dentro de interfaces. Para mayor entendimiento, tenemos el siguiente gráfico:

<div align="center"><img src="./images/mvp-diagram.png"/></div>

---

Regresemos al código:

Lo siguiente que vamos a hacer, es crear la interface presenterOutput en nuestra clase MovieService.

``` kotlin
interface MoviePresenterOutput {
	    fun showMovieInformation(movie: Movie?)
}
```

La interface va a tener un solo método por el momento, llamado `showMovieInformation`.

Lo siguiente que vamos a hacer es un método que va a permitir encontrar una película por su `id`. El código para esto es el siguiente:

``` kotlin
fun findMovieByName(name: String) {
	if (name.isNotBlank()) {
	    val movie = Movie(id = 1, name = name, overview = "Esta es una excelente pelicula.", posterPath = "https://image.tmdb.org/t/p/w500/wwemzKWzjKYJFfCeiB57q3r4Bcm.png")
	    presenterOutput.showMovieInformation(movie)
	} else {
	    presenterOutput.showMovieInformation(null)
	}
}
```

Ups! Tenemos un error, si revisamos nuestro gráfico de arquitectura, el siguiente paso es que nuestro service, debe recibir un atributo de tipo `MoviePresenterOutput` como parámetro del constructor.

``` kotlin
class MovieService(private val presenterOutput: MoviePresenterOutput)
``` 

Una vez hecho esto, nuestro servicio está listo para simular la devolución de una película.

Vamos ahora a trabajar en nuestro presenter:

Primero creamos el método que nos permita hacer la llamada al servicio que será el encargado de buscar la película.

``` kotlin
private val movieService = MovieService(this)
	
fun findMovieByName(name: String) {
     movieService.findMovieByName(name)
}
```

Esto nos da un error, y es que como dijimos, nuestro service recibe una instancia de `MoviePresenterOutput`, la palabra this es una referencia a si misma (`MoviePresenter`), entonces vamos a hacer que nuestro presenter herede de la interface `MoviePresenterOutput`.

``` kotlin
class MoviePresenter : MoviePresenterOutput
```

Nos está dando un error, sobre el nombre de la clase que indica que los métodos heredados no están siendo implementados. Entonces lo vamos a implementar de la siguiente manera:

``` kotlin
override fun showMovieInformation(movie: Movie?) {
	view.showMovieInformation(movie)
}
```

Pero qué es view?

Es la interface que debería estar definida en nuestro presenter. Entonces aquí hay dos acciones que realizar, igual que hicimos en nuestro service. La primera es crear la interface view dentro de nuestro presenter, y hacer que nuestro presenter reciba una instancia de view como parámetro.

Los bloques de código van a continuación:

``` kotlin
interface View {
	    fun showMovieInformation(movie: Movie?)
}
```

Luego de esto, la firma de nuestro presenter, debería estar definida de esta manera:

``` kotlin
class MoviePresenter(val view: View) : MoviePresenterOutput
```

Ahora, vamos a la capa de presentación. Tenemos ahorita una clase que se llama `MainActivity`, vamos a renombrarla, para que se llame `FindMovieActivity`. Y también vamos a renombrar al layout `activity_main` por `activity_find_movie`.

En el activity tenemos el método `onCreate` ya creado, como hablamos antes, el método `onCreate` es el primer método que se ejecuta en un activity. Dentro de este método podemos ver que tenemos una llamada al método `setContentView()`, este método siempre debe ser la primera llamada en el `onCreate`, ya que enlaza al layout con el Activity, para así poder usar los componentes.

Una vez dicho esto, lo primero que tenemos que hacer es crear la instancia al presenter.

``` kotlin
private val presenter = MoviePresenter(this)
```

Lo siguiente es hacer que nuestra Activity herede de la interface View que está en el presenter. Entonces la firma del artivity, queda así:

``` kotlin
class FindMovieActivity : AppCompatActivity(), MoviePresenter.View {
```

Una vez hecho esto, el último paso será implementar los métodos que ya estaban en la interface View.

``` kotlin
override fun showMovieInformation(movie: Movie?) {
   Toast.makeText(this,movie?.toString() ?: getString(R.string.movie_not_found), Toast.LENGTH_LONG).show()
}
```

Como se darán cuenta, nos hace referencia a movie_not_found. Esto como lo mencionamos antes, hace referencia a una cadena definida en el archivo strings. Entonces vamos a crearlo.

``` xml
<string name="movie_not_found">No existe informacion de esta pelicula</string>
```

Luego de esto, vamos a crear nuestro layout (activity_find_movie). Visualmente se va a ver así (No se rían, sabemos que es simple :P):

<div align="center"><img src="./images/activity-find-movie.png"/></div>

Solo es un campo de texto y un botón. El código para este layout es el siguiente:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <EditText
        android:id="@+id/edit_text_movie_name"
        android:layout_width="308dp"
        android:layout_height="63dp"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        android:ems="10"
        android:hint="@string/enter_movie_name"
        android:inputType="textPersonName"
        app:layout_constraintBottom_toTopOf="@+id/button_find_movie"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.503"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.789"
        android:gravity="center"/>

    <Button
        android:id="@+id/button_find_movie"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginBottom="228dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:text="@string/find_movie"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent" />

</android.support.constraint.ConstraintLayout>
```

Estamos usando un constraint layout. Este tipo de layout ayuda a que las interfaces sean responsivas en diferentes tamaños de pantallas, ya que el diseño se hace en base a los otros componentes de la pantalla.

También hay que crear nuevos strings en el archivo `strings.xml`

``` xml
<string name="enter_movie_name">Ingrese el nombre de la pelicula</string>
<string name="list_movies">Lista de Peliculas</string>
<string name="find_movie">Buscar Pelicula</string>
```

Ahora el siguiente paso, es relacionar estos componentes visuales con el código Kotlin. La acción que queremos en este caso es que cuando el usuario haga click en el botón buscar, la película se busque y se muestre el resultado en un toast (mensaje con cierto tiempo de duración en la pantalla). Esto lo vamos a hacer en el siguiente método.

``` kotlin
fun setActionsToScreenElements(){
	button_find_movie.setOnClickListener {
		presenter.findMovieByName(edit_text_movie_name.text.toString())
	}
}
```

Una vez que importen los elementos, los pueden usar. Uno hace referencia al botón y el otro al campo donde se ingresa el nombre de la película.
Button_find_movie y edit_text_movie son los ids de los elementos definidos en la UI (pueden echar un vistazo).
Este método debería ser llamado desde el método onCreate, ya que las acciones de los elementos de la pantalla, deben ser definidos al momento de crearla.
 Ah! Una cosa más, el método setActionsToScreenElements, debe estar definido en el view del presenter, ya que es una acción que puede realizarse bajos ciertos criterios, así que el presenter es quien tiene la responsabilidad de definir estas condiciones.

Entonces el view va a quedar así:

``` kotlin
interface View {
    fun setActionsToScreenElements()
    fun showMovieInformation(movie: Movie?)
}
```

Nuestro método en el Activity debe quedar así:

``` kotlin
override  fun setActionsToScreenElements(){
   button_find_movie.setOnClickListener {
          presenter.findMovieByName(edit_text_movie_name.text.toString())
    }
}
```

En el presenter, agregamos un método que nos permita llamar al nuevo método existente en el view.

``` kotlin
fun viewLoaded(){
    view.setActionsToScreenElements()
}
```

Con esto podemos entender, que este método va a ser llamado una vez que la vista finalmente sea cargada.

Finalmente, agregamos la llamada al método. Cuándo sería el momento adecuado?

Una vez que la vista esté cargada, es decir, después de ejecutar el método setContentView. Así:

``` kotlin
override fun onCreate(savedInstanceState: Bundle?) {
   super.onCreate(savedInstanceState)
   setContentView(R.layout.activity_find_movie)
   presenter.viewLoaded()
}
```

Estamos list@s para probar nuestra aplicación. Vamos a hacerlo!

<div align="center"><img src="./images/find-movie.png"/></div>

Listo, llegamos a nuestro primer checkpoint!! Te atrasaste?

Bueno tenemos una solución:

Ejecuta esto:

``` bash
Git checkout .
Git checkout 2019-version
Git checkout c39a14e71e8c105e6c550c951755117c8bca43d7
```

Ahora estás al día. Vamos con la siguiente etapa.

Nuestra segunda etapa, es la navegación. Vamos a ver cómo navegar entre activities y como pasar información entre una y otra.

Para esto, nuestro primer paso, va ser crear el activity.

En nuestra actividad anterior, estábamos imprimiendo la información de la película en un Toast. Ahora vamos a hacer que la información de la película se muestre en una pantalla diferente.

Para ello, primero vamos a crear un nuevo activity y su layout. Nuestro activity se va a llamar `MovieDetailActivity`.

El código inicial para esta clase, será el siguiente:

``` kotlin
package uio.androidbootcamp.moviesapp.view.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import uio.androidbootcamp.moviesapp.R

class MovieDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_movie_detail)
        super.onCreate(savedInstanceState)
    }
    
}
```
Ups, tenemos un error. El layout no existe aún. Vamos a crearlo. Queremos un layout que contenga una imagen y que nos permita mostrar el nombre y el overview de la película. El código para esto, es el siguiente:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <ImageView
        android:id="@+id/imageView"
        android:layout_width="384dp"
        android:layout_height="240dp"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.0"
        app:srcCompat="@color/colorAccent"
        android:contentDescription="@string/movie_image"/>

    <TextView
        android:id="@+id/text_view_movie_title"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.129"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/imageView"
        app:layout_constraintVertical_bias="0.096" />

    <TextView
        android:id="@+id/text_view_movie_release_date"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.129"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/imageView"
        app:layout_constraintVertical_bias="0.223" />

    <ScrollView
        android:id="@+id/scrollView2"
        android:layout_width="330dp"
        android:layout_height="210dp"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.514"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/text_view_movie_release_date"
        app:layout_constraintVertical_bias="0.916">

        <TextView
            android:id="@+id/text_view_movie_description"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />
    </ScrollView>

</android.support.constraint.ConstraintLayout>
```

Tenemos un error. Debido a que tenemos un recurso string que debe ser especificado en dicha clase. Vamos a poner la siguiente cadena en el archivo `strings.xml`

``` xml
<string name="movie_image">Movie Image</string>
```

Con esto, nuestro activity está creado.

Ahora para ver si funciona nuestro nuevo activity, vamos a hacer que el botón que teníamos en `FindMovieActivity`, navegue hacia nuestro nuevo activity.

Cambiemos nuestro método `setActionsToScreenElements`, va a quedar así:

``` kotlin
override  fun setActionsToScreenElements(){
    button_find_movie.setOnClickListener {
	// presenter.findMovieByName(edit_text_movie_name.text.toString())
        val intent = Intent(this, MovieDetailActivity::class.java)
        startActivity(intent)
    }
}
```

Una vez hecho esto, ejecutamos la aplicación.

Ohh no!! La aplicación se detiene. Nuestro primer crash. Porqué se produjo?

<div align="center"><img src="./images/app-error.png"/></div>

Vamos a ver donde se muestran los errores de las aplicaciones. Vamos a la ventana logcat, la cual está en la parte inferior de nuestro IDE.

<div align="center"><img src="./images/logcat.png"/></div>

Una vez que hacemos clic ahí, podemos ver que nos dice el log.

<div align="center"><img src="./images/error-log.png"/></div>

Bastante descriptivo, no? Como dijimos al inicio, todos nuestros activities deben estar registrados en el archivo manifest.xml

Agregamos la siguiente línea, justo debajo de la declaración de nuestro FindMovieActivity.

``` xml
<activity android:name=".view.activities.MovieDetailActivity" />
```

Una vez hecho esto, volvemos a correr la aplicación. Ahora la navegación si se da, pero se muestra una pantalla en blanco, ya que no tenemos datos.
Vamos a poner datos, y luego vamos a ver el código de navegación.

Al ser este un activity dependiente, no necesita obtener los datos de un repository. Los datos van a ser provistos por el activity que lo llama. Por tanto para este activity, solo vamos a tener, el presenter. El código para nuestro presenter será el siguiente:

``` kotlin
package uio.androidbootcamp.moviesapp.presenter

import android.content.Intent
import uio.androidbootcamp.moviesapp.model.models.Movie
import java.util.*

class MovieDetailPresenter(val view: MovieDetailView) {

    fun getMovie(intent: Intent) {
        val movie: Movie = intent.getSerializableExtra("movie") as Movie
        view.loadImage(movie.posterPath)
        view.showMovieTitle(movie.name)
        view.showMovieDate(movie.releaseDate)
        view.showMovieDescription(movie.overview)
    }
    
    interface MovieDetailView {
        fun loadImage(path: String)
        fun showMovieTitle(title: String)
        fun showMovieDate(date: Date)
        fun showMovieDescription(description: String)
    }
}
```

En esta clase, creamos un método que nos va a permitir obtener la película a partir de un intent, que es lo que se pasa de un activity a otro cuando se navega. Este objeto permite pasar información entre activities, además de básicamente definir el origen y el destino. Una vez que obtiene la película, ejecutará los métodos definidos en la interface view, es decir, cargar la imagen por medio de su path, mostrar el título, la fecha y la descripción de la película.

Luego de esto, continuamos con la modificación de nuestra clase `MovieDetailActivity`. El código de este activity quedaría por el momento así:

``` kotlin
package uio.androidbootcamp.moviesapp.view.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_movie_detail.*
import uio.androidbootcamp.moviesapp.R
import uio.androidbootcamp.moviesapp.presenter.MovieDetailPresenter
import java.util.*

class MovieDetailActivity : AppCompatActivity(), MovieDetailPresenter.MovieDetailView {

    private val presenter: MovieDetailPresenter = MovieDetailPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_movie_detail)
        super.onCreate(savedInstanceState)
        presenter.getMovie(intent)
    }

    override fun loadImage(path: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showMovieTitle(title: String) {
        text_view_movie_title.text = title
    }

    override fun showMovieDate(date: Date) {
        text_view_movie_release_date.text = date.toString()
    }

    override fun showMovieDescription(description: String) {
        text_view_movie_description.text = description
    }
}
```

Aquí a grandes rasgos, lo que hacemos es implementar los métodos del view para hacer las acciones necesarias. Pero nos falta el método `loadImage` que aún no está implementado.

Antes de la implementación de este método, hay cosas que debemos implementar en `FindMovieActivity` para poder pasar los datos de manera correcta a `MovieDetailActivity`.

Primero en `MoviePresenter`, vamos a modificar nuestra interface `View` para que tenga los métodos necesarios.

Quedando así:

``` kotlin
interface View {
	fun setActionsToScreenElements()
	fun showMovieInformation(movie: Movie)
	fun showMovieNotFoundMessage()
}
```

Nuestro método `showMovieInformation` debería quedar así:

``` kotlin
override fun showMovieInformation(movie: Movie?) {
	if (movie == null) {
		view.showMovieNotFoundMessage()
	} else {
		view.showMovieInformation(movie)
	}
}
```

Ahora en nuestra clase `FindMovieActivity` debemos implementar lo siguiente:

``` kotlin
override fun showMovieInformation(movie: Movie) {
	val intent = Intent(this, MovieDetailActivity::class.java)
	intent.putExtra("movie", movie)
	startActivity(intent)
}

override fun showMovieNotFoundMessage() {
	Toast.makeText(this, getString(R.string.movie_not_found), Toast.LENGTH_LONG).show()
}

override  fun setActionsToScreenElements(){
	button_find_movie.setOnClickListener {
		presenter.findMovieByName(edit_text_movie_name.text.toString())
	}
}
```
Para poder enviar un objeto en el intent, este debe ser serializable, por lo que vamos a ir a nuestra clase Movie y vamos a indicarle que es una clase serializable, así:

``` kotlin
data class Movie (val id: Long = 0, val name: String = "", val posterPath: String = "", val overview: String = "", val releaseDate: Date = Date()) : Serializable
```

Una vez hecho esto, el error que teníamos va a desaparecer.

Vimos que tenemos la palabra "movie" en nuestro intent. Esta es una clave para obtener un dato específico y debe ser el mismo donde se envía y donde se recibe. Para eliminar el riesgo, vamos a hacer que este valor esté en una constante. Vamos a crear la clase `Contants` en el paquete `utils`. Así:

``` kotlin
package uio.androidbootcamp.moviesapp.utils

object Constants {
    val MOVIE = "MOVIE"
}
```

Luego vamos a reemplazar esta constante en los sitios donde estaba utilizada la palabra movie.

A continuación vamos a ocuparnos de nuestra imagen:

Vamos a agregar la librería Fresco (https://frescolib.org) para el manejo de imágenes. Por qué?

Esta es una muy buena librería que ayuda a que la carga de imágenes de manera asíncrona. Además nos ayuda a que las imágenes de internet sean cargadas directamente con el URL y no con otras implementaciones más complicadas. Vamos a agregar la siguiente dependencia en nuestro build.gradle.

``` gradle
implementation 'com.facebook.fresco:fresco:1.9.0'
```

Una vez hecho esto, hacemos un sync del proyecto, y tendremos la librería disponible.

Ahora vamos a sobreescribir la clase aplicación. Esta clase es la que carga dependencias y configuraciones iniciales de la aplicación. Solo debe ser sobreescrita de ser necesaria una funcionalidad. En este caso, fresco necesita inicializarse con la aplicación. Entonces vamos a crear el paquete app. Dentro de este vamos a crear la clase `customApplication`, la cual en su método `onCreate` va a inicializar Fresco. Así:

``` kotlin
package uio.androidbootcamp.moviesapp.app

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class CustomApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }
}
```

Ahora necesitamos agregar un permiso para poder usar internet. Esto lo agregamos en nuestro `manifest.xml`, sobre el tag application.

``` xml
 <uses-permission android:name="android.permission.INTERNET" />
```

En el mismo archivo, reemplazamos nuestra clase de aplicación por la clase recientemente creada.

El tag application lo reemplazamos por esto:

``` xml
<application
        android:name=".app.CustomApplication"
```

Ahora vamos a ir al layout `activity_movie_detail`. Reemplazamos nuestro `imageView` con el `imageView` de Fresco.

El layout va a quedar así:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent">


    <com.facebook.drawee.view.SimpleDraweeView
        android:id="@+id/image_view_movie_poster"
        android:layout_width="384dp"
        android:layout_height="240dp"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.0"
        app:srcCompat="@color/colorAccent"
        android:contentDescription="@string/movie_image"/>

    <TextView
        android:id="@+id/text_view_movie_title"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.129"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/image_view_movie_poster"
        app:layout_constraintVertical_bias="0.096" />

    <TextView
        android:id="@+id/text_view_movie_release_date"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.129"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/image_view_movie_poster"
        app:layout_constraintVertical_bias="0.223" />

    <ScrollView
        android:id="@+id/scrollView2"
        android:layout_width="330dp"
        android:layout_height="210dp"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.514"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/text_view_movie_release_date"
        app:layout_constraintVertical_bias="0.916">

        <TextView
            android:id="@+id/text_view_movie_description"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />
    </ScrollView>

</android.support.constraint.ConstraintLayout>
```

Luego de esto, ya el último paso en `MovieDetailActivity`, vamos a implementar nuestro método pendiente, `loadImage`, así:

``` kotlin
override fun loadImage(path: String) {
	image_view_movie_poster.setImageURI(path)
}
```

Una vez hecho esto, estamos listos para correr nuestra aplicación. Veamos que tenemos!

<div align="center"><img src="./images/movie-detail.png"/></div>

La interfaz no está del todo bien. Pero nos muestra datos.

No se preocupen, ya iremos a la cuestión de los estilos.

Te atrasaste? Solo sigue estos pasos y vas a estar al día:

``` bash
Git checkout .
Git checkout  4138d59adf1c9b056e511ee749bb4867a6645051
```

## Consumo de REST en Android

Ahora la siguiente parte, tiene que ver con el consumo de servicios web. Pero qué son?

<div align="center"><img src="./images/rest-api-arch.jpg"/></div>

Son una vía de comunicación entre dos computadores a través de internet. 
Como funciona?
A grandes rasgos, el cliente envía una solicitud con ciertos datos al servidor y el servidor le responde con otros datos, los cuales serán utilizados para realizar alguna acción específica.
Por qué Rest?
Son servicios mucho más rápidos, así que para las aplicaciones móviles son perfectos, ya que en este tipo de aplicaciones, la velocidad no es sólo una opción o algo prescindible.

Si quieres saber más sobre web services, este es un buen artículo:

https://www.tutorialspoint.com/webservices/what_are_web_services.htm

Bueno y qué vamos a usar?

Retrofit and Gson.

### Retrofit

Esta librería (https://square.github.io/retrofit/) nos permite consultar datos desde un servicio rest. Hace muy fácil la implementación de estos consumidores como ya veremos a continuación.

### Gson

Nos permite transformar objetos desde Json a objetos Kotlin. Esto es muy útil, ya que así podemos manejar de mejor manera los datos que tenemos en nuestros servicios web (https://github.com/google/gson)

Lo primero que haremos es agregar las dependencias de las librerías a nuestro archivo build.gradle. Agregamos las siguientes líneas en el bloque de dependencias.

``` gradle
    implementation ("com.squareup.retrofit2:retrofit:2.5.0")
    implementation "com.squareup.retrofit2:converter-gson:2.5.0"

```

Antes de seguir con la implementación, veamos el API que vamos a usar. Es el siguiente:
https://www.themoviedb.org/documentation/api
https://developers.themoviedb.org/3/search/search-movies


Esta Url provee muchos endpoints que podemos usar. Debemos obtener una key para poder trabajar con este API, así que por favor obtengan su API key y por favor, recuerden no publicarlo en sus repositorios de Github.
Podemos seguir las instrucciones de esta dirección para obtener el API key:
https://developers.themoviedb.org/3/getting-started/introduction

Una vez hecho esto, podemos continuar con la implementación.

Ahora, una vez hecho esto, vamos a crear una interface que nos permita hacer las llamadas rest. Las ponemos en una interface, ya que solo necesitamos sus firmas, con esto, retrofit puede hacer ya las llamadas necesarias. Entonces en nuestro movieService, agregamos la siguiente interface:

``` kotlin
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface MovieRestServices {
   @GET("movie/")
   fun findMovieByName(@QueryMap options: Map<String, String>): Call<MovieWrapper>
}
```

El método que vamos a utilizar es search/movie. Pueden ver su estructura en la página de la documentación del API.

Si se fijan en la respuesta que da el API, nos regresa un objeto, que dentro tiene los resultados en una lista llamada results. Por lo que necesitamos un wrapper para poder convertir estas respuestas. Para esto, crearemos la clase MovieWrapper, el código es el siguiente:

``` kotlin
data class MovieWrapper(val results: List<Movie>)
```

Una vez hecho esto, vamos a crear la instancia de retrofit que nos va a permitir interactuar con el API. El código es el siguiente:

``` kotlin
package uio.androidbootcamp.moviesapp.model.services

import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit

object RetrofitInstance { 
    private const val BASE_URL = "http://api.themoviedb.org/3/search/"
    
    val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}
```

En este caso, este es un object, es decir un valor estático. A la instancia de retrofit le pasamos la URL base del API y en el converter factory, usamos Gson. Esto va a permitir que convierte la respuesta del servicio web al objeto Kotlin que nosotros estamos esperando.

Ahora la nueva implementación de nuestro método `findMovieByName` será la siguiente:

``` kotlin
    fun findMovieByName(name: String) {
        val options = mapOf("api_key" to "api_key_to_replace", "query" to name)
        val call = movieRestService.findMovieByName(options)
        println(call.request())
        call.enqueue(manageFindMovieResponse())
    }

    private fun manageFindMovieResponse(): Callback<MovieWrapper> {
        return object : Callback<MovieWrapper> {
            override fun onResponse(call: Call<MovieWrapper>, response: Response<MovieWrapper>) {
                response.body()?.let {
                    if (it.results.isNullOrEmpty()) {
                        presenterOutput.showMovieInformation(null)
                    }
                    presenterOutput.showMovieInformation(it.results[0])
                }
            }

            override fun onFailure(call: Call<MovieWrapper>, error: Throwable) {
                error.printStackTrace()
                presenterOutput.showMovieInformation(null)
            }
        }
    }
```

La variable options es un mapa clave valor, cuyas claves serán `api_key` y `query`.

Nota: `api_key_to_replace` Debe ser reemplazada por la clave obtenenida en la página del API que estamos utilizando.

Luego se hace la llamada, la cual quedará en el objeto call. Luego de esto, por medio del método enqueue se va a manejar la respuesta de manera asíncrona. Es decir, lanzamos la petición y una vez que se resuelva, ejecutamos el caso exitoso (`onResponse`) o el caso de falla (`onFailure`). Estos métodos son parte de la interface Callback.

Para tener la variable `movieRestService`, debemos pasarla por el constructor de nuestra clase `MovieService`. Cuya firma quedará así:

``` kotlin
class MovieService(private val presenterOutput: MoviePresenterOutput, private val movieRestService: MovieRestServices) {
```

En la interface View, creamos el siguiente método:

``` kotlin
fun getMovieService(): MovieService
```

Luego en la clase `MoviePresenter`, reemplazamos la inicialización de la variable `movieService`, con la siguiente:

``` kotlin
private lateinit var movieService: MovieService
```

Y por último, el método `viewLoaded` dentro de `MoviePresenter`, cuya implementación quedará así:

``` kotlin
fun viewLoaded() {
   movieService = view.getMovieService()
   view.setActionsToScreenElements()
}
```

Con esto nos aseguramos de obtener una instancia válida de movieService cuando el activity esté cargado.

Luego, en `FindMovieActivity`, debemos implementar el método, el cual quedaría de la siguiente manera:

``` kotlin
override fun getMovieService(): MovieService {
val movieRestService : MovieService.MovieRestServices = RetrofitInstance.retrofit.create(MovieService.MovieRestServices::class.java)
return MovieService(presenter, movieRestService)
}
```

Debemos cambiar nuestra clase Movie para que pueda obtener los valores desde el servicio web. La clase quedaría así:

``` kotlin
data class Movie(val id: Long = 0, val title: String, @SerializedName("poster_path") val posterPath: String, val overview: String, @SerializedName("release_date") val releaseDate: Date = Date()) : Serializable
```

`@SerializedName`, sirve para mapear los campos como llegan del servicio web a nuestros campos.

Vamos a la clase `MovieDetailPresenter`, en el método `getMovie`, vamos a reemplazar las siguientes líneas:

``` kotlin
view.loadImage(movie.posterPath)
view.showMovieTitle(movie.name)
```

Con estas:

``` kotlin
view.loadImage("https://image.tmdb.org/t/p/w200"+movie.posterPath)
view.showMovieTitle(movie.title)
```

Esto con el fin de obtener una imagen de un tamaño que podamos manejar correctamente, y cambiar el campo `name` por `title`, ya que también lo hicimos en nuestro modelo.

Ahora por último en nuestro layout `activity_movie_detail.xml`, vamos a reemplazar esta línea en el `SimpleDraweeView`:

``` xml
android:scaleType="fitStart"
```

Con esta:

``` xml
app:actualImageScaleType="fitXY"
```

Esto va a hacer que la imagen se centre de mejor manera.

Vamos a probar la aplicación:

<div align="center"><img src="./images/avengers-search.png"/></div>

<div align="center"><img src="./images/avengers-detail.png"/></div>

Listo, consumimos nuestro primer servicio rest. 

Te atrasaste? Solo ejecuta estos comandos.

``` sh
git checkout .
git checkout 55206cb56c7610a9be21765cdbea6e8a2ee119ee
```

## Vista de Películas

Ahora vamos a ver todo un catálogo de películas, para que el usuario pueda seleccionarlas y ver el detalle de las mismas a su elección.

Para ello, lo primero será agregar la librería necesaria, en el archivo `build.gradle`

``` gradle
implementation 'com.android.support:recyclerview-v7:27.1.1'
```

Luego de esto, vamos a crear el método para obtener las películas desde el API, nuestra interface `MovieRestServices` quedará así:

``` kotlin
interface MovieRestServices {
  @GET("search/movie/")
  fun findMovieByName(@QueryMap options: Map<String, String>): Call<MovieWrapper>

  @GET("movie/popular")
  fun findPopularMovies(@QueryMap options: Map<String, String>): Call<MovieWrapper>
}
```

Esta interface, por el momento, está creada como una interface interna dentro de la clase `MovieService`. Vamos a mover esta interface a ser una clase individual, dentro del paquete services. Esto va a requerir que corrijamos los imports donde esta interface haya estado siendo usada. Lo mismo vamos a hacer para la clase `movieWrapper`, pero  esta clase será trasladada dentro del paquete `model.models`.

Necesitamos hacer un cambio en la URL base, ya que antes la estábamos tomando hasta `search/`, pero ahora vamos a hacer un query de otro dominio, por lo tanto, search va sólo en un método. Para esto, en la clase `RetrofitInstance`, vamos a cambiar la url, que va a quedar así:

``` kotlin
private const val BASE_URL = "http://api.themoviedb.org/3/"
```

Ahora vamos a crear la clase `MovieListService`. Esta clase nos permitirá resolver las llamadas rest necesarias para esta nueva pantalla y notificará al presenter una vez que esté completado el procesamiento de las llamadas Rest. El código será el siguiente:

``` kotlin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uio.androidbootcamp.moviesapp.model.models.Movie
import uio.androidbootcamp.moviesapp.model.models.MovieWrapper


class MovieListService(private val presenterOutput: MovieListPresenterOutput, private val movieRestService: MovieRestServices) {

    fun findMostPopularMovies() {
        val options = mapOf("api_key" to "api_key_to_replace")
        val call = movieRestService.findPopularMovies(options)
        println(call.request())
        call.enqueue(manageFindPopularMoviesResponse())
    }

    private fun manageFindPopularMoviesResponse(): Callback<MovieWrapper> {
        return object : Callback<MovieWrapper> {
            override fun onResponse(call: Call<MovieWrapper>, response: Response<MovieWrapper>) {
                response.body()?.let {
                if (it.results.isNullOrEmpty()) {
                    presenterOutput.showMovies(null)
                }
                presenterOutput.showMovies(it.results)
                }
            }


            override fun onFailure(call: Call<MovieWrapper>, error: Throwable) {
                error.printStackTrace()
                presenterOutput.showMovies(null)
            }
        }
    }


    interface MovieListPresenterOutput {
        fun showMovies(movies: List<Movie>?)
    }
}
```

Ahora vamos a necesitar crear un nuevo presenter, para que se encargue de manejar la lógica de la pantalla que mostrará la lista de películas. Para esto, crearemos la clase `MovieListPresenter`, la cual va a tener un método `loadMovies`, el cual será el encargado de notificar a la vista, cuando las películas estén cargadas, esto se hará por medio de la interface view en el presenter, tal como lo hemos venido haciendo. El código quedaría así:

``` kotlin
import uio.androidbootcamp.moviesapp.model.models.Movie
import uio.androidbootcamp.moviesapp.model.services.MovieListService

class MovieListPresenter(val view: MovieListView) : MovieListService.MovieListPresenterOutput {

    private lateinit var movieListService: MovieListService
    var movies = mutableListOf<Movie>()
        private set

    fun viewLoaded() {
        movieListService = view.getMovieListService()
    }

    fun loadMovies() {
        movieListService.findMostPopularMovies()
    }

    override fun showMovies(movies: List<Movie>?) {
        when (movies) {
            null -> view.showMovieRetrieveError()
            else -> {
                this.movies.addAll(movies)
                view.updateRecyclerView()
            }
        }
    }

    interface MovieListView {
        fun updateRecyclerView()
        fun showMovieRetrieveError()
        fun getMovieListService(): MovieListService
    }
}
```

Ahora vamos a crear la clase `MovieListActivity`. Primero antes de poner el código Kotlin, vamos a crear el layout, ya que este nos permitirá tener los elementos visuales a los que debemos acceder desde el activity. EL código del layout, al que llamaremos `activity_movie_list.xml`, es el siguiente:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">

        <android.support.v7.widget.RecyclerView
            android:id="@+id/recycler_view_movies"
            android:layout_width="match_parent"
            android:layout_height="match_parent" />

    </LinearLayout>
</android.support.constraint.ConstraintLayout>
```

El código del activity, será el siguiente:

``` kotlin
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import uio.androidbootcamp.moviesapp.R
import uio.androidbootcamp.moviesapp.model.services.MovieListService
import uio.androidbootcamp.moviesapp.model.services.MovieRestServices
import uio.androidbootcamp.moviesapp.model.services.RetrofitInstance
import uio.androidbootcamp.moviesapp.presenter.MovieListPresenter
import uio.androidbootcamp.moviesapp.view.adapters.MovieListAdapter

class MovieListActivity : AppCompatActivity(), MovieListPresenter.MovieListView {

    private val presenter = MovieListPresenter(this)

    private lateinit var recyclerViewMovies: RecyclerView
    private lateinit var moviesAdapter: MovieListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_movie_list)
        super.onCreate(savedInstanceState)
        presenter.viewLoaded()
    }

    override fun configureRecyclerView() {
        moviesAdapter = MovieListAdapter(presenter.movies, presenter.onItemSelected())
        recyclerViewMovies = findViewById(R.id.recycler_view_movies)
        recyclerViewMovies.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = moviesAdapter
        }
        presenter.loadMovies()
    }

    override fun updateRecyclerView() {
        moviesAdapter.notifyDataSetChanged()
    }

    override fun showMovieRetrieveError() {
        Toast.makeText(this, getString(R.string.movies_cannot_be_retrieved), Toast.LENGTH_LONG).show()
    }

    override fun getMovieListService(): MovieListService {
        val movieRestService : MovieRestServices = RetrofitInstance.retrofit.create(MovieRestServices::class.java)
        return MovieListService(presenter, movieRestService)
    }
}
```

Esta clase por el momento tiene algunos errores, los cuales vamos a corregir con los siguientes bloques de código. Por el momento, analicemos el método `configureRecyclerView`.

Todo recyclerview necesita un adapter para funcionar. El adapter es la clase que permite que el recyclerview “pinte” los datos en la pantalla. Será la siguiente clase que vamos a implementar.

La siguiente línea, nos permite saber cuál es el objeto visual, que corresponde al recyclerview. Esta manera de hacerlo no siempre es necesaria, como ya vimos hace en los activities anteriores, pero es bueno conocerla, porque a veces se necesita que el enlace entre el elemento kotlin y el elemento visual no se puede hacer de inicio, sino en un momento cuando este aparezca.

Luego con el método `apply`, se pasa todas las configuraciones necesarias al recyclerview. Primero le decimos que el tamaño del recyclerview no va a cambiar. Es decir que haya 10 o 1000 elementos, el tamaño del recyclerview en pantalla será el mismo. Luego de esto, el layout manager, nos permite decirle cómo organizaremos nuestros elementos, en este caso, va a ser de manera lineal, vertical por defecto. Por último se especifica el adapter que se va a usar.

Ahora el código del adapter es el siguiente:

``` kotlin
import android.support.v7.widget.RecyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.view.SimpleDraweeView
import uio.androidbootcamp.moviesapp.R
import uio.androidbootcamp.moviesapp.model.models.Movie
class MovieListAdapter(private val moviesList: List<Movie>, private val onItemSelected: View.OnClickListener) :
        RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.movie_list_item, parent, false)
        val imageView = view.findViewById(R.id.movie_list_image) as SimpleDraweeView
        imageView.setOnClickListener(onItemSelected)
        return ViewHolder(view)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.findViewById(R.id.movie_list_image) as SimpleDraweeView

    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imageView.setImageURI("""https://image.tmdb.org/t/p/w200${moviesList[position].posterPath}""")
        holder.imageView.tag = moviesList[position].id
    }
}
```

En este adapter, tenemos métodos relevantes que analizar:

El método `onCreateViewHolder`. Este método nos va a ayudar a manejar cada elemento de la lista. En este se define la interfaz que tendrá cada elemento y las acciones que tendrán. Nosotros en este caso, vamos a tener una imagen, y se podrá hacer click en cada una de ellas, para poder navegar y ver sus detalles. 

Una vez hecho esto, retornamos una instancia de `viewHolder`, que en este caso es la clase que tiene el `imageview`. 

El método `onBindViewHolder`, se ejecuta una vez por cada elemento que se vaya a pintar en la pantalla. En este caso, lo que hace es pintar la imagen correspondiente a cada elemento en la pantalla, y almacena la posición de cada elemento en un tag, para poder saber cuál elemento fue el seleccionado por el usuario.

El adapter recibe una lista de películas, que es su fuente de datos. Y el otro parámetro es una interface del tipo `View.OnClickListener`, esta nos permitirá manejar las acciones que se realizarán al hacer click en los elementos de la lista desde fuera, ya que en esta capa no debemos tener lógica alguna.

Ahora creamos el layout necesario para esto, se va a llamar `movie_list_item.xml`.

``` xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="400dp"
    xmlns:app="http://schemas.android.com/apk/res-auto">


    <com.facebook.drawee.view.SimpleDraweeView
        android:id="@+id/movie_list_image"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:scaleType="fitXY">

    </com.facebook.drawee.view.SimpleDraweeView>

</android.support.constraint.ConstraintLayout>
```

Ahora agregamos el siguiente elemento al archivo `strings.xml`:

``` xml
<string name="movies_cannot_be_retrieved">No se pudieron obtener peliculas, por favor intente nuevamente</string>
```
Luego de esto, creamos el método onItemSelected en `MovieListPresenter` con el siguiente código:

``` kotlin
import android.view.View    
fun onItemSelected(): View.OnClickListener {
    return View.OnClickListener { v ->
        val id = v?.tag as Long
        val movie = movies.firstOrNull { it.id == id }
        movie?.let { view.showMovieInformation(it) }
    }
}
```

Este método lo que hace es obtener el id de la película seleccionada, buscarlo en la lista de películas y sólo si existe, decirle al activity que muestre la información de esta película.

Luego de esto, será necesario agregar el método `showMovieInformation` y el método `configureRecyclerView` a la interface `movieListView`, así:

``` kotlin
fun showMovieInformation(movie: Movie)
fun configureRecyclerView()
```

El método `configureRecyclerView` debe ser ejecutado cuando la interface ha sido cargada, es decir en el método `viewLoaded` de `movieListPresenter`, el cuál quedará así:

``` kotlin
fun viewLoaded() {
    movieListService = view.getMovieListService()
    view.configureRecyclerView()
}
```

Ahora por último debemos implementar el método `showMovieInformation` dentro de `movieListActivity`, entonces agregamos el siguiente método:

``` kotlin
override fun showMovieInformation(movie: Movie) {
    val intent = Intent(this, MovieDetailActivity::class.java)
    intent.putExtra(Constants.MOVIE, movie)
    startActivity(intent)
}
```

Este método nos va a permitir mostrar el detalle de la película, en la pantalla `MovieDetailActivity` que creamos anteriormente. 

Cómo último paso, registramos el nuevo activity en el archivo `manifest` y vamos a hacer que este sea el punto de entrada a la aplicación, para esto el código del manifest va a quedar así:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="uio.androidbootcamp.moviesapp">

    <uses-permission android:name="android.permission.INTERNET" />

    <application
        android:name=".app.CustomApplication"
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">
        <activity android:name=".view.activities.FindMovieActivity">
        </activity>
        <activity android:name=".view.activities.MovieDetailActivity" />
        <activity android:name=".view.activities.MovieListActivity" >
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
    </application>

</manifest>
```

Bueno, esto ha sido largo, vamos a probarlo! Ejecutemos la aplicación:

<div align="center"><img src="./images/recycler-view.png"/></div>

Listo!! Podemos probar la navegación.

Vamos a empezar con nuestro siguiente tema, pero antes, si te atrasaste, solo ejecuta estos comandos:

``` sh
git checkout .
git checkout 6ab284e01e49d03d5b85c03b9b386a49403f9526
```

## Almacenamiento Local (Shared Preferences)

La última parte que vamos del tutorial, es hacer que nuestra aplicación pueda guardar favoritos. Para esto lo podemos hacer con base de datos o con almacenamiento local.
En este caso vamos a utilizar el almacenamiento local, conocido como Shared Preferences.

Hay que tomar en cuenta, que en el almacenamiento en memoria solo podemos guardar ciertos tipos de datos. No podemos guardar objetos, por lo cual, vamos a almacenar las películas en formato JSON, es decir, serializadas.

Otra cosa a tomar en cuenta, es que el almacenamiento es un clave, valor, como se maneja un mapa.

Una vez dicho esto, vamos a empezar. Primero creamos el siguiente elemento en el archivo `strings.xml`:

``` xml
<string name="package_name">uio.androidbootcamp.moviesapp</string>
```

Ahora vamos a crear la clase que nos permitirá manipular el shared preferences. Para esto, crearemos la clase `SharedPreferencesRepository`, en el paquete repositories, dentro de model:

``` kotlin
package uio.androidbootcamp.moviesapp.model.repositories

import android.content.Context
import uio.androidbootcamp.moviesapp.R


class SharedPreferencesRepository(val context: Context) {

    fun saveStringInSharedPreferences(key: String, value: String) {
        val sharedPref = context.getSharedPreferences(
                context.resources.getString(R.string.package_name), Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getStringFromSharedPreferences(key: String): String {
        val sharedPref = context.getSharedPreferences(
                context.resources.getString(R.string.package_name), Context.MODE_PRIVATE)
        return sharedPref.getString(key, "")
    }
}
```

Esta clase, tiene dos métodos, uno que guardará strings y otro que los puede recuperar. Una parte interesante aquí, es como se obtiene el objeto `sharedPreferences`.

Se debe especificar un valor único para la aplicación, así, todo lo que pertenece a ella, será almacenado y solo podrá recuperado por la misma. Es por esto, que como identificador, escogimos el nombre del paquete (id) de nuestra aplicación.

Ahora, vamos a crear el repositorio de películas, que nos permitirá realizar almacenar y recuperar las películas. Vamos a reemplazar el contenido de la clase `MovieRepository`. La misma quedará así:

``` kotlin
package uio.androidbootcamp.moviesapp.model.repositories

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import uio.androidbootcamp.moviesapp.model.models.Movie
import uio.androidbootcamp.moviesapp.utils.Constants

//Manejo de repositorios de base de datos
class MovieRepository(private val sharedPreferencesRepository: SharedPreferencesRepository) {

    private val gson = createGsonObject()
    private val key = Constants.MOVIE

    fun getMoviesFromSharedPreferences(retrieveMovieOutput: RetrieveMovieOutput) {
        val movies = retrieveMovies()
        retrieveMovieOutput.showMovies(movies)
    }

    fun saveMovieInSharedPreferences(value: Movie, saveMovieOutput: SaveMovieOutput) {
        val moviesToSave = mutableListOf<Movie>()
        val moviesFromSharedPreferences = retrieveMovies()
        val filteredMovies = moviesFromSharedPreferences?.filter { it.id == value.id }
        if (filteredMovies.isNullOrEmpty()) {
            if (!moviesFromSharedPreferences.isNullOrEmpty()) {
                moviesToSave.addAll(moviesFromSharedPreferences)
            }
            moviesToSave.add(value)
            saveMoviesInSharedPreferences(moviesToSave)
            saveMovieOutput.isSaved(true)
        }else {
            saveMovieOutput.isSaved(false)
        }
    }

    private fun saveMoviesInSharedPreferences(value: List<Movie>) {
        val jsonValue = gson.toJson(value)
        sharedPreferencesRepository.saveStringInSharedPreferences(key, jsonValue)
    }

    private fun retrieveMovies(): List<Movie>? {
        val listType = object : TypeToken<ArrayList<Movie>>() {
        }.type
        val gsonValue = sharedPreferencesRepository.getStringFromSharedPreferences(key)
        return Gson().fromJson<List<Movie>>(gsonValue, listType)
    }

    private fun createGsonObject(): Gson {
        return GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()
    }


    interface SaveMovieOutput {
        fun isSaved(isSaved: Boolean)
    }

    interface RetrieveMovieOutput {
        fun showMovies(movies: List<Movie>?)
    }
}
```

Esta clase tiene dos métodos públicos, uno para guardar una película y otro para recuperar una lista de películas. Estas son las únicas funciones que vamos a necesitar por ahora. El método guardar, solo va a guardar una película, si es que esta no está guardada aún, caso contrario, notificará sobre la existencia previa de la película.

Una curiosidad, porqué tenemos dos interfaces para este repositorio?

Al ser un repositorio, va a ser accedido por uno o más presenters, por lo cual no podemos tener una sola interface.  Por tanto, vamos a usar las instancias de las interfaces, únicamente en los métodos en que son necesarias.

Ahora, lo primero que vamos a hacer, es implementar la funcionalidad para agregar una película como favorita. Para ello, vamos a agregar un botón de favorito a la pantalla `MovieDetailActivity`.

El primer paso será hacer que nuestro `MovieDetailPresenter`, sea capaz de guardar películas. Para ello agregamos lo siguiente:

Primero, la firma de nuestra clase `MovieDetailPresenter`, quedará así:

``` kotlin
class MovieDetailPresenter(val view: MovieDetailView, val movieRepository: MovieRepository) : MovieRepository.SaveMovieOutput {
```

Ahora, dentro de esta clase, agregamos los siguientes métodos:

``` kotlin
fun viewLoaded(){
    view.setActionsToScreenElements()
}
    
fun saveFavoriteMovie() {
    movieRepository.saveMovieInSharedPreferences(movie, this)
}

override fun isSaved(isSaved: Boolean) {
    if (isSaved) {
        view.showSavedSuccessfullyMessage()
    } else {
        view.showMovieIsAlreadySavedMessage()
    }
}
```

Uno de ellos, es el método que llamaremos desde el activity, el método is saved, es el método de la interfaz, una vez que se guarde la película, ejecuta uno u otro método del view según la respuesta.

Por último, agregamos tres métodos a la interface `MovieDetailView`:

``` kotlin
fun showSavedSuccessfullyMessage()
fun showMovieIsAlreadySavedMessage()
fun setActionsToScreenElements()
```

Ahora por último, reemplazamos el método getMovie, por lo siguiente:

``` kotlin
    lateinit var movie: Movie

    fun getMovie(intent: Intent) {
        movie = intent.getSerializableExtra(MOVIE) as Movie
        view.loadImage("https://image.tmdb.org/t/p/w200" + movie.posterPath)
        view.showMovieTitle(movie.title)
        view.showMovieDate(movie.releaseDate)
        view.showMovieDescription(movie.overview)
    }
```

Con esto terminamos el presenter. Ahora vamos a la parte más interesante, el activity. Vamos a crear un botón flotante, este nos va a permitir hacer favorita la película.

Primero, vamos a agregar una dependencia necesaria en el archivo `build.gradle.`

``` gradle
implementation 'com.android.support:design:27.1.1'
```

Esta librería nos permitirá hacer uso del botón flotante.

Luego de esto vamos al archivo `activity_movie_detail.xml`. 

En la parte final del archivo, debajo del componente `scrollView`, agregamos el siguiente elemento:

``` xml
<android.support.design.widget.FloatingActionButton
        android:id="@+id/fab_save_movie"
        android:layout_width="60dp"
        android:layout_height="60dp"
        android:layout_gravity="end|bottom"
        android:layout_marginStart="16dp"
        android:layout_marginTop="5dp"
        android:layout_marginEnd="8dp"
        android:layout_marginBottom="40dp"
        android:src="@android:drawable/ic_menu_add"
        android:scaleType="fitCenter"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.908"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/scrollView2" />
```

Esto agrega el elemento, y lo pone en la esquina inferior derecha de la pantalla.
Ahora vamos a agregar el código en `MovieDetailActivity`, para que este componente pueda funcionar.

En el método onCreate, agregamos la siguiente llamada al método del presenter que indica las acciones a realizar cuando la interface ya fue cargada:

``` kotlin
presenter.viewLoaded()
```

Luego, agregamos las siguientes implementaciones para los nuevos métodos que definimos anteriormente en el view:

``` kotlin
override fun showMovieIsAlreadySavedMessage() {
    Toast.makeText(this, R.string.movie_not_saved, Toast.LENGTH_LONG).show()
}

override fun showSavedSuccessfullyMessage() {
    Toast.makeText(this, R.string.movie_saved, Toast.LENGTH_LONG).show()
}

override fun setActionsToScreenElements() {
    fab_save_movie.setOnClickListener { presenter.saveFavoriteMovie() }
}
```

Por último, en el archivo strings.xml, agregamos los siguientes recursos:

``` xml
<string name="movie_saved">Pelicula almacenada con exito</string>
<string name="movie_not_saved">La película ya está guardada como favorita</string>
```

En MovieDetailActivity actualizar la declaración del presenter y agregar dos líneas antes de eso. Quedaría de la siguiente manera:

``` kotlin
private val sharedPreferencesRepository = SharedPreferencesRepository(this)
private val movieRepository = MovieRepository(sharedPreferencesRepository)
private val presenter: MovieDetailPresenter = MovieDetailPresenter(this, movieRepository)
```

Ahora ya podemos ejecutar la aplicación, el resultado será el siguiente:

<div align="center"><img src="./images/captain-marvel.png"/></div>

Y como veo mi lista de películas? Vamos a crear una pantalla que nos permita ver esta lista de películas que guardamos como favoritas. 

Primero, vamos a crear el presenter, que nos permita manejar la lógica de esta pantalla. Se va a llamar `FavoriteMovieListPresenter`.

``` kotlin
package uio.androidbootcamp.moviesapp.presenter

import android.view.View
import uio.androidbootcamp.moviesapp.model.models.Movie
import uio.androidbootcamp.moviesapp.model.repositories.MovieRepository

class FavoriteMovieListPresenter(val view: FavoriteMovieListView, private val movieRepository: MovieRepository) : MovieRepository.RetrieveMovieOutput {

    var movies = mutableListOf<Movie>()
        private set

    fun viewLoaded() {
        view.configureRecyclerView()
    }

    fun loadMovies() {
        movieRepository.getMoviesFromSharedPreferences(this)
    }

    fun onItemSelected(): View.OnClickListener {
        return View.OnClickListener { v ->
            val id = v?.tag as Long
            val movie = movies.firstOrNull { it.id == id }
            movie?.let { view.showMovieInformation(it) }
        }
    }

    override fun showMovies(movies: List<Movie>?) {
        when (movies) {
            null -> view.showMovieRetrieveError()
            else -> {
                this.movies.addAll(movies)
                view.updateRecyclerView()
            }
        }
    }

    interface FavoriteMovieListView {
        fun updateRecyclerView()
        fun showMovieRetrieveError()
        fun showMovieInformation(movie: Movie)
        fun configureRecyclerView()
    }
}
```

Este presenter es bastante similar a `MovieListPresenter`. Pero como ya habíamos hablado anteriormente, un presenter no debe ser usado para dos activities. Por tanto, escribiremos este nuevo presenter. El cual como podemos observar, en su método `loadMovies`, carga las películas desde el almacenamiento local.

Lo siguiente va a ser diseñar el adapter, el cual va a permitir mostrar los datos formateados en la lista de favoritos que crearemos más adelante. EL código es el siguiente:

``` kotlin
package uio.androidbootcamp.moviesapp.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import uio.androidbootcamp.moviesapp.R
import uio.androidbootcamp.moviesapp.model.models.Movie

class FavoriteMovieListAdapter(private val moviesList: List<Movie>, private val onItemSelected: View.OnClickListener) :
        RecyclerView.Adapter<FavoriteMovieListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.favorite_movie_list_item, parent, false)
        val movieContainer = view.findViewById(R.id.rl_favorite_movie_container) as RelativeLayout
        movieContainer.setOnClickListener(onItemSelected)
        return ViewHolder(view)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView = view.findViewById(R.id.text_view_movie_name) as TextView
        val movieContainer = view.findViewById(R.id.rl_favorite_movie_container) as RelativeLayout

    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = moviesList[position].title
        holder.movieContainer.tag = moviesList[position].id
    }
}
```

La estructura de esto, ya la vimos anteriormente. Entonces Solo vamos a hacer énfasis en que únicamente se va a mostrar el título de la película y la zona clickeable va a ser todo el contenedor. 

El siguiente paso es crear el layout para cada item dentro de la lista. Dentro de la carpeta layouts, vamos a crear el archivo `favorite_movie_list_item.xml`.

``` xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/rl_favorite_movie_container"
    android:layout_width="match_parent"
    android:layout_height="150dp"
    android:layout_marginStart="8dp"
    android:layout_marginTop="8dp"
    android:layout_marginEnd="8dp"
    app:layout_constraintEnd_toEndOf="parent"
    app:layout_constraintStart_toStartOf="parent"
    app:layout_constraintTop_toTopOf="parent">

    <TextView
        android:id="@+id/text_view_movie_name"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_centerInParent="true"
        android:textSize="20sp"/>

    <View
        android:layout_width="match_parent"
        android:layout_height="2dp"
        android:layout_alignParentBottom="true"
        android:background="@android:color/background_dark"/>

</RelativeLayout>
```

Este layout tiene un textView donde se va a mostrar el nombre de la película y un view que usaremos como divisor entre los elementos de la lista.
Con esto, ya no tenemos errores en el adapter. Por lo que lo damos por finalizado. Ahora vamos a crear el activity, al cual vamos a llamar `FavoriteMovieListActivity`.

> Para crear el activity, se puede seguir el método de crearlo con la asistencia del IDE, que hicimos anteriormente. Otra forma es crearlo manualmente, sin la ayuda del IDE. Cualquiera de las dos está bien.

El activity, una vez creado, tendrá el siguiente código:

``` kotlin
package uio.androidbootcamp.moviesapp.view.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import uio.androidbootcamp.moviesapp.R
import uio.androidbootcamp.moviesapp.model.models.Movie
import uio.androidbootcamp.moviesapp.model.repositories.MovieRepository
import uio.androidbootcamp.moviesapp.model.repositories.SharedPreferencesRepository
import uio.androidbootcamp.moviesapp.presenter.FavoriteMovieListPresenter
import uio.androidbootcamp.moviesapp.utils.Constants
import uio.androidbootcamp.moviesapp.view.adapters.FavoriteMovieListAdapter

class FavoriteMovieListActivity : AppCompatActivity(), FavoriteMovieListPresenter.FavoriteMovieListView {

    private val sharedPreferencesRepository = SharedPreferencesRepository(this)
    private val movieRepository = MovieRepository(sharedPreferencesRepository)
    private val presenter = FavoriteMovieListPresenter(this, movieRepository)

    private lateinit var recyclerViewMovies: RecyclerView
    private lateinit var moviesAdapter: FavoriteMovieListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_favorite_movie_list)
        super.onCreate(savedInstanceState)
        presenter.viewLoaded()
    }

    override fun configureRecyclerView() {
        moviesAdapter = FavoriteMovieListAdapter(presenter.movies, presenter.onItemSelected())
        recyclerViewMovies = findViewById(R.id.recycler_view_favorite_movies)
        recyclerViewMovies.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = moviesAdapter
        }
        presenter.loadMovies()
    }

    override fun updateRecyclerView() {
        moviesAdapter.notifyDataSetChanged()
    }

    override fun showMovieRetrieveError() {
        Toast.makeText(this, getString(R.string.movies_cannot_be_retrieved), Toast.LENGTH_LONG).show()
    }

    override fun showMovieInformation(movie: Movie) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(Constants.MOVIE, movie)
        startActivity(intent)
    }
}
```

Este activity está atado al layout `activity_favorite_movie_list.xml`. Este Activity implementa la interface `FavoriteMovieListView`, y por tanto sus métodos, los cuales nos permitirán actualizar el RecyclerView, mostrar un error en el caso de ser necesaio, navegar a la pantalla de detalles y el más complejo que es la configuración del RecyclerView, que es muy parecida a la que ya hicimos anteriormente.

El layout para esta interface, llamado `activity_favorite_movie_list`, tendrá el siguiente código:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <android.support.v7.widget.RecyclerView
        android:id="@+id/recycler_view_favorite_movies"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />

</android.support.constraint.ConstraintLayout>
```

Este layout es bastante simple. Solo tiene un RecyclerView que ocupará toda la pantalla.

Si crearon el activity de manera manual, no se olviden de registrar el activity en el `manifest.xml`.

``` xml
<activity android:name=".view.activities.FavoriteMovieListActivity"/>
```

Con esto ya podríamos ver nuestra lista de películas. Pero, cómo hacemos?

No podemos modificar a cada momento el archivo `manifest.xml`, ya que sería un poco incómodo. Para esto existe otra opción rápida de implementar, un **menú**.

Para esto, vamos a crear dentro de la carpeta res, la carpeta menu.
Luego dentro de esta carpeta, creamos el archivo `navigation_menu.xml`.

Dentro de este archivo, ponemos el siguiente contenido:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:id="@+id/menu_navigate_to_find_movie"
        android:title="@string/find_movie_by_name" />
    <item android:id="@+id/menu_navigate_to_movies_list"
        android:title="@string/popular_movies" />
    <item android:id="@+id/menu_navigate_to_favorite_movies_list"
        android:title="@string/favorite_movies" />
</menu>
```

El contenido de este archivo es un menú, que contiene items, los cuales en este caso únicamente tendrán un título.

Ahora agregamos los siguientes elementos en el archivo `strings.xml`.

``` xml
<string name="favorite_movies">Películas favoritas</string>
<string name="popular_movies">Películas más populares</string>
<string name="find_movie_by_name">Buscar pelicula por nombre</string>
```

Una vez hecho esto, vamos a agregar los siguientes métodos a `MovieListActivity`, ya que ahora, esta es la clase que se muestra al iniciar la aplicación, y será nuestro punto de entrada a la misma. Agregamos los siguientes métodos:

``` kotlin
override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.navigation_menu, menu)
    return true
}

override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
        R.id.menu_navigate_to_find_movie -> {
            startActivity(Intent(applicationContext, FindMovieActivity::class.java))
            true
        }
        R.id.menu_navigate_to_favorite_movies_list -> {
            startActivity(Intent(applicationContext, FavoriteMovieListActivity::class.java))
            true
        }
        else -> {
            startActivity(Intent(applicationContext, MovieListActivity::class.java))
            finish()
            true
        }
    }
}
```

El método `onCreateOptionsMenu`, permite indicar cuál es el archivo de menú que se va a usar. Nosotros usaremos el que creamos anteriormente.

Luego el método `onOptionsItemSelected` permite definir las acciones que se realizarán con cada item del menú. En este caso serán navegaciones a otras clases. Y listo. Podemos probar la aplicación.

El menú se verá así:

<div align="center"><img src="./images/menu.png"/></div>

Y la pantalla de películas favoritas (después de que agreguen sus favoritas), se verá así:

<div align="center"><img src="./images/favourite-movies.png"/></div>

Listo! Hemos cubierto todos los temas que teníamos planeados hasta hoy. Si quieres la aplicación en este punto, solo ejecuta lo siguiente:

``` sh
git checkout .
git checkout 2019-version
```

Te quedó tiempo libre? Tenemos 2 retos extras si quieres improvisar un poco.

1. Hacer que en la lista de películas populares, se muestren dos películas en cada fila en lugar de una.

2. Cambiar los títulos de cada pantalla, para que podamos ver de mejor manera la funcionalidad a la que corresponde cada pantalla.
